---

- name: Ensure jitsi is deployed
  hosts: label_docker_:&label_environment_{{ env }}
  remote_user: root
  tasks:
    - name: Ensure project dir exists
      file:
        path: /root/jitsi
        state: directory

    - name: Ensure jitsi deployment files are present
      synchronize:
        src: "{{ playbook_dir }}/"
        dest: /root/jitsi/

    - name: Ensure .env is deployed
      copy: 
        src: /root/jitsi/env.example
        dest: /root/jitsi/.env
        remote_src: true
        force: false

    - name: Configure jitsi domain
      lineinfile:
        path: /root/jitsi/.env
        line: "PUBLIC_URL=https://meet.{{ lookup('env', 'BASE_DOMAIN') }}"
        regexp: '^PUBLIC_URL='

    - name: Ensure screensharing has 30fps
      lineinfile:
        path: /root/jitsi/.env
        line: "DESKTOP_SHARING_FRAMERATE_{{ item }}=60"
        regexp: "^DESKTOP_SHARING_FRAMERATE_{{ item }}="
      loop:
        - "MIN"
        - "MAX"

    - name: Disable bitrate cap for screensharing
      lineinfile:
        path: /root/jitsi/.env
        line: "TESTING_CAP_SCREENSHARE_BITRATE=0"
        regexp: '^TESTING_CAP_SCREENSHARE_BITRATE'

    - name: Ensure DNS is set up
      inwx.collection.dns:
        domain: "{{ lookup('env', 'BASE_DOMAIN') }}"
        type: "A"
        record: "meet"
        value: "{{ ansible_default_ipv4.address }}"
        solo: true
        ttl: "3600"
        username: "{{ lookup('passwordstore', env +'/DNS/INWX_USER')}}"
        password: "{{ lookup('passwordstore', env +'/DNS/INWX_PASSWORD')}}"
      delegate_to: localhost

    - name: Ensure secrets are generated
      command: /root/jitsi/gen-passwords.sh

    - name: Ensure jitsi is deployed
      docker_compose:
        state: present
        project_src: /root/jitsi
      environment:
        - BASE_DOMAIN: "{{ lookup('env', 'BASE_DOMAIN') }}"

